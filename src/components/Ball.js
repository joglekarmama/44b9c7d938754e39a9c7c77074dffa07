import React,{ useState, useEffect, useRef } from 'react'
/*import Screen2 from './Screen2'*/

const HEIGHT = 480;
const WIDTH = 640;

const Ball = ({RewardImage})=> {
    var [startX, setStartX]=useState(0)
    var [startY, setStartY]=useState(0)
var  [isDrawing, setIsDrawing]=useState(false)
    const canvas1=React.createRef()
    useEffect((e)=> {
       const canvas = canvas1.current;
       const context = canvas.getContext("2d");
        
       /*const canvas=document.getElementsByClassName('ball')[0]*/
        canvas.addEventListener("mousedown", scratchStart);
        canvas.addEventListener("mousemove", scratch);
        canvas.addEventListener("mouseup", scratchEnd);

       /* canvas.addEventListener("touchstart", scratchStart, {passive:true});
    canvas.addEventListener("touchmove", scratch, {passive:true});
    canvas.addEventListener("touchend", scratchEnd, {passive:true});*/
    
   
    context.fillStyle = "";
    
   context.fillRect(0, 0, WIDTH, HEIGHT);
    context.lineWidth = 60;
    context.lineJoin = "round";
   
        
    })
    
    function scratchStart(e) {
        const {layerX, layerY} = e;
        console.log(layerX, layerY)
        setIsDrawing(true)
        setStartX(layerX)
        setStartY(layerY)
        
    }
   
    function scratch(e){
        setTimeout(function(){
    
    const { layerX, layerY } = e;
    
        const context = canvas1.current.getContext("2d");
        
        if (!isDrawing) {
            return;
          }
          context.globalCompositeOperation = "destination-out";
          /*canvas.clearRect( 0 , 0 ,layerX, layerY );*/
          context.beginPath();
         
          context.moveTo(startX, startY);
          context.lineTo(layerX, layerY);
          context.closePath();
          context.stroke();
          document.getElementsByClassName('ball')[0].style.backgroundImage="url("+RewardImage+")"
          document.getElementsByClassName('ball')[0].style.animation="move-eye-skew 1s ease-in"
          document.getElementsByTagName('button')[0].style.visibility="visible"
          setStartX(layerX);
          setStartY(layerY)
          document.getElementsByTagName('h3')[0].style.visibility="hidden";
          document.getElementsByTagName('h2')[0].style.visibility='visible'
        },1000)
    }
    
    function scratchEnd(e) {
        setIsDrawing(false)
      };
   /* function scratch1() {
        
        document.getElementsByClassName('ball')[0].style.backgroundImage="url("+RewardImage+")"
        document.getElementsByClassName('ball')[0].style.animation="move-eye-skew 1s ease-in"
        document.getElementsByTagName('button')[0].style.visibility="visible"
        
        
    }*/
    

    /*function scratchStart() {
console.log('helo')
    }*/
    return(
        <>
       {/*<canvas id="canvas" />*/}
      
        <canvas  ref={canvas1} className='ball'/>
       
       
       
        </>
    )
}
export default Ball